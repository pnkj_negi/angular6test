import { Injectable } from '@angular/core';
import { HttpService } from './common/services/http.service';
import { Http, Response } from '@angular/http';
import { LocalStorage } from './common/services/local-storage.service';
import {environment} from '../environments/environment';
import * as _ from 'lodash';
import { catchError, map } from 'rxjs/operators';
@Injectable()
export class AppService {

  constructor(private _http: HttpService, private http: Http) {
  }

  login(userObj){
    return this._http.post(`${environment.url}/login/authenticatebyemail?email=${userObj.email}&password=${userObj.password}`).pipe(map(res => res.json()));
  }

  signup(userObj){
    return this._http.post(`${environment.url}/login/save`, userObj).pipe(map(res => res.json()));
  }

 forgotPassword(userObj){
    return this._http.put(`${environment.url}/users/forgotPassword`, userObj).pipe(map(res => res.json()));
  }

  setPassword(userObj){
    return this._http.put(`${environment.url}/users/setPassword`, userObj).pipe(map(res => res.json()));
  }
  verifyEmail(userObj){
    return this._http.put(`${environment.url}/users/verifyEmail`, userObj).pipe(map(res => res.json()));
  }


 
 
}

