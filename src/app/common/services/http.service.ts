import { Http, Headers, ConnectionBackend, Response, RequestOptionsArgs, Request, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable, EMPTY, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class HttpService extends Http {
  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private router: Router) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return this.authInterceptor(super.request(url, this.appendAuthHeader(options)));
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.authInterceptor(super.get(url, this.appendAuthHeader(options)));
  }

  post(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.authInterceptor(super.post(url, body, this.appendAuthHeader(options)));
  }

  put(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.authInterceptor(super.put(url, body, this.appendAuthHeader(options)));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.authInterceptor(super.delete(url, this.appendAuthHeader(options)));
  }

  private appendAuthHeader(options?: RequestOptionsArgs): RequestOptionsArgs {
    const mergedOptions: RequestOptionsArgs = Object.assign({ headers: new Headers({
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
    }) }, options);
    const auth = localStorage.getItem('access_token');
    if (auth ) {
      mergedOptions.headers.append('x-access-token', 'qwerty123456789');
      mergedOptions.headers.append('Content-Type', 'application/json');
    } else {
      mergedOptions.headers.append('Content-Type', 'application/json');
    }
    return mergedOptions;
  }

  private authInterceptor(observable: Observable<Response>): Observable<Response> {
    return observable.pipe(
        catchError( err => {
             if (err.status == 401) {
                 this.router.navigateByUrl('/login');
                 return EMPTY;
             } else {
                 return throwError(err);
             }
        })
    );
  }
}
