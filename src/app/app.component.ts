import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ToasterConfig} from 'angular2-toaster';
import { LoaderService } from './common/services/loader.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public config: ToasterConfig =
  new ToasterConfig({
    showCloseButton: true,
    tapToDismiss: false,
    timeout: 2500,
    animation: 'fade',
    positionClass: ' toast-top-right custom-toast'
  });
  constructor(private router: Router) { }

  ngOnInit() {
  }
  title = 'Testing Application';
  public redirect(type) {
    this.router.navigate([type]);

  }
}
