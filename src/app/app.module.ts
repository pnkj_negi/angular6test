import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

//routing
import { Router } from '@angular/router';

// components


import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent} from './signup/signup.component';
import { DashboardComponent} from './dashboard/dashboard.component';
import {ForgotPasswordComponent } from './forgot-password/forgot-password.component'

//services
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { LoaderService } from './common/services/loader.service';
import { LocalStorage } from './common/services/local-storage.service';
import { AppService } from './app.service';
import { HttpService } from './common/services/http.service';
import { RequestOptions, Http, XHRBackend } from '@angular/http';

// HTTP services Exported
export function httpFactory(backend: XHRBackend, defaultOptions: RequestOptions, router: Router, toasterService: ToasterService) {
  return new HttpService(backend, defaultOptions, router);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ShowHidePasswordModule,
    ToasterModule.forRoot(),
    AppRoutingModule
  ],
  providers: [{
    provide: HttpService,useFactory: httpFactory,deps: [XHRBackend, RequestOptions, Router]},
    { provide: LocalStorage, useValue: 'test' },ToasterService, LoaderService,AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
