import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AppService } from '../app.service';
import { LoaderService } from '../common/services/loader.service';
import { ToasterService } from 'angular2-toaster';
import { ToasterConfig} from 'angular2-toaster';
import { LocalStorage } from '../common/services/local-storage.service';
import { Validations } from '../constants/validation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public spiner: boolean;
	public loginForm: any;
	public submitted:Boolean = false;
	validations =Validations;
	constructor(@Inject(FormBuilder) fb: FormBuilder, private appService: AppService, private toasterService: ToasterService, private loaderService: LoaderService,private router:Router) {
		this.loginForm = fb.group({
			emailId: ['', Validators.required],
			password: ['', Validators.required],
		});
  }
  
  ngOnInit() {

	}

  signin() {
		this.submitted=true;
		if (this.loginForm.valid) {
			this.loaderService.display(true);
			const data ={
			       	email: this.loginForm.controls.emailId.value,
                password:this.loginForm.controls.password.value
			}
			this.appService.login(data).subscribe((data) => {
				this.loaderService.display(false);
				// this.spiner = false;
				//console.log(data);
				this.router.navigate(['/dashboard']);
				// LocalStorage.set('access-token',data['access-token']);
				this.toasterService.pop('success', data.message);
			},
				(err) => {
					this.loaderService.display(false);
					// this.spiner = false;
					const errMessage = JSON.parse(err._body).message;
					this.toasterService.pop('error', errMessage);
						//console.log(err);

				})
		} else {
			//console.log('invalid form');

		}
	}

}