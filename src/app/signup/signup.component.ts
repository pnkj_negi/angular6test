import { Component, OnInit, Inject } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { map } from 'rxjs/operators';
import {environment} from '../../environments/environment';
import { AppService } from '../app.service';
import { LoaderService } from '../common/services/loader.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public signupForm:any;
    public emailPattern = '^[A-Za-z0-9]+([\-_\.][A-Za-z0-9]+)*@([A-Za-z0-9]([A-Za-z0-9\-]{0,61}[A-Za-z0-9])?\.)+[A-Za-z]{2,6}$';
    public spiner:Boolean = false;
    public submitted:Boolean = false;
    constructor(@Inject(FormBuilder) fb: FormBuilder,private appService: AppService,private toasterService: ToasterService, private loaderService: LoaderService, private router:Router) {
      this.signupForm = fb.group({
        name: ['', Validators.required],
        emailId: ['', Validators.required],
        password: ['', Validators.required],
      });
    }
  
    ngOnInit() {
    
    }

    signup(){
      // //console.log(this.user);
      this.submitted=true;
      if (this.signupForm.valid) {
        // this.spiner =true;
        this.loaderService.display(true);
        // this.spiner = true;
        const data ={
          fullname: this.signupForm.controls.name.value,
          email: this.signupForm.controls.emailId.value,
          password: this.signupForm.controls.password.value,
         
        }
        this.appService.signup(data).subscribe((data) => {
           this.loaderService.display(false);
          // this.spiner = false;
          console.log(data);
          
          this.router.navigate(['/login']);
           this.toasterService.pop('success', data.message);
        },
          (err) => {
            this.loaderService.display(false);
            // this.spiner = false;
            this.toasterService.pop('error', 'Network Issue');
            //console.log(err);
  
          })
      } else {
        //console.log('invalid form');
  
      }
    }

  }