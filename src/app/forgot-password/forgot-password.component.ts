import { NgModule, Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AppService } from '../app.service';
import { LocalStorage } from '../common/services/local-storage.service';
import { LoaderService } from '../common/services/loader.service';
import { ToasterService } from 'angular2-toaster';
@Component({
	selector: 'app-forgot-password',
	templateUrl: './forgot-password.component.html',
	styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

	public spiner: boolean;
	public submitted: Boolean = false;
	public form: any;
	constructor(@Inject(FormBuilder) fb: FormBuilder, private appService: AppService, private router: Router, private http: Http, private toasterService:ToasterService, private loaderService:LoaderService) {
		this.form = fb.group({
			emailId: ['', Validators.required]
		});
	}

	ngOnInit() {

	}

	forgotPassword() {
		this.submitted = true;
		if (this.form.valid) {
			this.loaderService.display(true);
			// this.spiner = true;
			this.appService.forgotPassword(this.form.value).subscribe((data) => {
				this.loaderService.display(false);
				// this.spiner = false;
				//console.log(data);
				LocalStorage.clear();
				this.router.navigate(['/login']);
				// LocalStorage.set('access-token', data['access-token']);
				this.toasterService.pop('success', data.message);
			},
				(err) => {
					this.loaderService.display(false);
					// this.spiner = false;
					// this.toasterService.pop('error', 'Network Issue');
					const errMessage = JSON.parse(err._body).message;
					this.toasterService.pop('error', errMessage);
					//console.log(err);

				})
		} else {
			//console.log('invalid form');

		}
	}
}