export const Validations = {
emailPattern : '^[A-Za-z0-9]+([\-_\.][A-Za-z0-9]+)*@([A-Za-z0-9]([A-Za-z0-9\-]{0,61}[A-Za-z0-9])?\.)+[A-Za-z]{2,6}$',
generalPattern: '^(?! )[A-Za-z0-9 .!@#$&()-_&*~`]*(?! )$',
passwordPattern: '^(?!.* ).{8,15}$',
}